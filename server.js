var express = require("express");
var app     = express();
var path    = require("path");

app.use('/src', express.static(path.join(__dirname, 'src')));

app.get('/',function(req,res){  // req = requisição; res = resposta
    res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/produtos',function(req,res){
    res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/cadastro',function(req,res){
    res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/carrinho',function(req,res){
    res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/meuspedidos',function(req,res){
    res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/pedido',function(req,res){
    res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname+'/404.html'));
});

app.listen(process.env.PORT || 30000);

console.log('rodando na por 30000')