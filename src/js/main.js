    /* VALORES DOS CÓDIGOS DE ESTADO HTTP */
var READY_STATE_COMPLETE = 4,   // Estado completo, já foi recebida a resposta do servidor
    STATUS_OK = 200,    // Requisição deu certo
    ACESS_DENIED = 401, // Requisição teve acesso negado
    SERVER_ERROR = 500, // Ocorreu um erro no servidor durante a requisição
    /* LINKS */
    indexCliente = 'http://e-commercesmnweb.azurewebsites.net', // Link da página inicial
    APIdotNet = 'https://e-commercesmndotnet.azurewebsites.net', // API DOTNET
    APInodeJs = 'https://e-commercenodejs.azurewebsites.net',    // API NODEJS
    APIseguranca = 'https://ecommercesmn.azurewebsites.net',    // API SEGURANÇA
    webCliente = 'http://e-commercesmnweb.azurewebsites.net',
    /* VARIÁVEIS */
    contCarrinho,   // Contador de itens no carrinho
    buscaProd,  // Barra de pesquisa no cabeçalho
    loginModal, // Modal de login/perfil
    loginModalContent,  // Conteúdo do modal de login/perfil
    nomeUsuario,    // Nome do usuário logado
    bemVindo,   // Div com mensagem de Bem-Vindo no cabeçalho
    erroModal,  // Modal de erro
    erroModalContent,   // Conteúdo do modal de erro
    buscaCabecalho, // Barra de busca no cabeçalho
    btnMeusPedidos, // Botão Meus Pedidos no login
    sair;   // Botão de logout no modal de login/perfil

/* COMANDOS QUE SERÃO EXECUTADOS AO CARREGAR A PÁGINA */
$(document).ready(function() { 
    $.get('http://e-commercesmnweb.azurewebsites.net/src' + (location.pathname == '/' ? '/produtos' : location.pathname) + '.html')
    // $.get('http://localhost:30000/src' + (location.pathname == '/' ? '/produtos' : location.pathname) + '.html')
        .then(function (res) {
            $('#main-content').append(res);
            load();
        })
});
window.addEventListener('load', function () {
    contCarrinho = document.getElementById('contCarrinho');
    buscaProd = document.getElementById('buscaProd');
    loginModal = document.getElementById('loginModal');
    loginModalContent = document.getElementById('loginModalContent');
    bemVindo = document.getElementById('bemVindo');
    erroModal = document.getElementById('erroModal');
    erroModalContent = document.getElementById('erroModalContent');
    buscaCabecalho = document.getElementById('buscaCabecalho');
    modalBuscaCabecalho = document.getElementById('modalBuscaCabecalho');
    if(localStorage.nome){  // Verifica se o usuário está logado
        nomeUsuario = localStorage.getItem('nome');
        bemVindo.innerHTML = 'Bem vindo, <a onclick="showLogin()">' + capitalize(nomeUsuario) + '</a>';
    }
});

/* FUNÇÕES ATIVADAS POR CLICK NA TELA */
window.addEventListener('click', function (event) {
    if (event.target == loginModal) {
        loginModal.style.display = 'none';
    }
    if (event.target == document.getElementById("btnFecha")) {
        erroModal.classList.remove("centro");
        dialogoModal.classList.remove("centro");
    }
    if (event.target == modalBuscaCabecalho) {
        expandBusca();
    }
});

/* FUNÇÕES PARA BARRA DE BUSCA */
function expandBusca() {   // Função que expande a barra de busca
    if (buscaCabecalho.style.position == "absolute") {
        buscaCabecalho.removeAttribute("style");
        modalBuscaCabecalho.classList.remove('background-modal');
        modalBuscaCabecalho.classList.remove('topo');
        modalBuscaCabecalho.style.margin = "0 5%";
    }
    else if (window.innerWidth <= 600) {
        modalBuscaCabecalho.className = "background-modal topo";
        modalBuscaCabecalho.style.margin = "0";
        buscaCabecalho.style.position = "absolute";
        buscaCabecalho.style.width = "80%";
        buscaCabecalho.style.margin = "18px 0";
        buscaCabecalho.style.zIndex = "2";
        buscaProd.focus();
    }
}

/* FUNÇÕES DE LOGIN */
function showLogin() {  // Mostra o modal de login
    if (localStorage.nome) {
        loginModal.style.display = 'block';
        loginModalContent.innerHTML = '<h1 id="userName"><i class="material-icons md-36">person</i>' + capitalize(nomeUsuario) +'</h1> <button class="btn btn-contained btnLogin" id="meusPedidos">MEUS PEDIDOS</button><button class="btn btnLogin btn-contained" id="logout">SAIR</button>';
        sair = document.getElementById('logout');   // Botão de logout no modal de login/perfil
        sair.addEventListener('click',logout);
        btnMeusPedidos = document.getElementById("meusPedidos");    // Botão Meus pedidos no modal de login/perfil
        btnMeusPedidos.addEventListener('click',meusPedidos);
    }
    else {
        loginModal.style.display = 'block';
        loginModalContent.innerHTML = '<h1><i class="material-icons md-36">person</i>Login</h1>            <p id="avisoLogin" class="aviso"></p>            <form method="post" id="formLogin" onsubmit="return sendLogin()">            <p>E-mail</p>            <input class="input-padrao" type="email" id="emailLogin" required maxlength="100" autofocus />            <p>Senha</p>            <input class="input-padrao" type="password" id="senhaLogin" required minlength="6" maxlength="20" />            <input type="submit" class="btn btnLogin btn-contained" value="ENTRAR" />            <p class="dir">Cliente novo? <a href="cadastro">Cadastre-se</a></p>        </form>';
        var formLogin = document.getElementById('formLogin'),  // Formulário (e-mail e senha) no modal de login
            avisoLogin = document.getElementById('avisoLogin'), // Mensagem de erro no modal de login
            emailLogin = document.getElementById('emailLogin'), // Campo de e-mail no modal de login
            senhaLogin = document.getElementById('senhaLogin'); // Campo de senha no modal de login
        formLogin.addEventListener('keypress', function (e) {   // Enviar login ao pressionar a tecla Enter
            var key = e.which || e.keyCode;
            if(key === 13) {
                sendLogin();
            }
        });
        emailLogin.focus();
    }
}
function sendLogin() {  // Chama uma requisição http passando a rota de login e o e-mail e senha informados
    var route = APIseguranca + '/api/public/cliente/login',
        data = {
        'email': emailLogin.value.toLowerCase(),
        'senha': senhaLogin.value
    };
    setRequestPost(estadoLogin, route, data);
    return false;
}
function estadoLogin(){ // Função relacionada ao estado da requisição http do login
    if(this.readyState == READY_STATE_COMPLETE) {
        if(this.status == STATUS_OK) {  // Requisição deu certo
            var json_data = JSON.parse(this.responseText);
            localStorage.setItem('token', json_data.token); // Armazena o token do usuário no localStorage
            localStorage.setItem('nome', json_data.usuario.nome);   // Armazena o nome do usuário no localStorage
            location.reload();
        }
        else if(this.status == ACESS_DENIED) {  // Acesso negado
            var json_data = JSON.parse(this.responseText);
            avisoLogin.innerHTML = json_data.message;   // Mostra mensagem de erro
            if(localStorage.token){ // Caso o usuário possua um token inválido ele será deslogado
                logout();
                location.reload();
            }
        }
        else if(this.status == SERVER_ERROR) {  // Erro no servidor
            var json_data = JSON.parse(this.responseText);
            erroModal.className = 'background-modal centro';
            erroModalContent.innerHTML = '<h1>Erro - '+ json_data.code +'</h1>            <h2>'+ json_data.message +'</h2>            <p>Tente novamente mais tarde</p>            <button class="btn btn-contained" onclick="closeModalErro()">FECHAR</button>'
        }
    }
}
function logout() { // Desloga o usuário
    localStorage.removeItem('nome');
    localStorage.removeItem('token');
    location.reload();
}

/* PEDIDOS */
function meusPedidos(){
    location.href = 'meuspedidos';
}

/* FUNÇÕES CARRINHO */
function attContCarrinho() {    // Atualiza o contador de itens no carrinho
    if (localStorage.qtdeProd > 0) {
        contCarrinho.innerHTML = localStorage.qtdeProd;
    }
    else {
        contCarrinho.innerHTML = 0;
    }
}

/* REQUISIÇÕES HTTP */
var meuNavegador = function() { // Verifica se o navegador é compatível com XMLHttpRequest (O IE utiliza ActiveXObject)
    var nav = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    return nav;
}
var setRequestPost = function(callback, route, data) {   // Parâmetros: (callback = função que irá interagir com a resposta recebida pelo servidor, route = a rota por onde será feita a requisição, data = dados do tipo JSON que serão enviados para o servidor)
    var req = meuNavegador();
    data = JSON.stringify(data);    // Os dados devem ser enviados como string para o servidor
    req.onreadystatechange = callback;  // O método onreadystatechange é ativado toda vez que o estado da requisição for alterado
    req.open('POST', route, true); // O método open é o que irá abrir a conexão com o servidor; Parâmetros: (method = método http pelo qual será feita a requisição, route = rota por onde vai ser enviada a informação, boolean = determina se a requisição vai ser assíncrona ou sincrona)
    req.setRequestHeader('Content-Type','application/json');
    req.send(data); // Envia a requisição com os dados
}
var setRequestGetAuth = function(callback, route) { // Request com verificação do token do usuário logado
    var req = meuNavegador();
    req.onreadystatechange = callback;
    req.open('GET', route, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));  // Envia o token do usuário logado para verificar sua validade
    req.send();
}

/* NOTIFICAÇÕES */
function showNotif(msg,valid) {
    var avisoNotif = document.getElementById('avisoNotif');
    if(valid){
        avisoNotif.innerHTML = '<th style="color:#04BD04">'+ msg +'</th>';
    }
    else {
        avisoNotif.innerHTML = '<th style="color:red">'+ msg +'</th>';
    }
    document.getElementById('notificacao').style.display = "block";
    document.getElementById('notificacao').className = "notificacoes";
    setTimeout(closeNotif, 5500);
}
function closeNotif() {
    document.getElementById('notificacao').style.display = "none";
    document.getElementById('notificacao').classList.remove('notificacoes');
}

/* FUNÇÕES GERAIS */
function capitalize(nome) { // Função que capitaliza o nome (primeira letra maiúscula)
    var nome = nome.split(" ");
    for(var i = 0; i < nome.length; i++) {
        if(nome[i].trim().length != 0){
            nome[i] = nome[i][0].toUpperCase() + nome[i].substring(1).toLowerCase();
        }
    }
    if(nome.length > 1){
        return nome[0] + " " +nome[nome.length-1];
    }
    else {
        return nome;
    }
}
function closeModalErro() { // Função que fecha o modal de erro
    if (erroModal != null) {
        erroModal.classList.remove('centro');
    }
}
function toMoney(value) {   // Função que transforma o valor recebido para o formato 999.999,99 (reais)
    var position, tam, negative = false;
    if (typeof(value) != typeof(1)) { // Se o valor recebido não for um número, retornará o próprio valor
        return value;
    }
    value = value.toFixed(2);   // Formata o valor à duas casas decimais (9999.99999 => 9999.99)
    value = value.toString();   // Transforma o valor em string (9999.99 => "9999.99")
    value = value.replace('.', ',');    // Substitui os pontos por vírgulas ("9999.99" => "9999,99")
    tam = value.length; // Armazena a quantidade de caracteres do valor ("9999,99" => 7)
    if (value.search('-') > -1) {   // Verifica se o valor é negativo
        negative = true;    // Variável que informa se o número inserido é negativo
        value = value.slice(1, tam);    // Remove o sinal de negativo do número ("-9999,99" => "9999,99")
    }
    position = (value.length - 3);  // Armazena a quantidade de caracteres da parte inteira do produto ("9999,99" => 4)
    if (tam > 6) {
        while (position - 3 > 0) {
            position -= 3;
            value = value.slice(0, position) + '.' + value.slice(position, tam);    // Insere um ponto a cada 3 dígitos ("9999,99" => "9.999,99")
            tam++;  // Aumenta o tamanho pois acrescentou o ponto
        }
    }
    value = negative ? ('-' + value) : (value); // Se o número for negativo adiciona o sinal negativo no valor
    return value;
}