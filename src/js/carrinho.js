'use strict';
var prodCarrinho,   // tbody da tabela de produtos no carrinho
    servCarrinho,   // tbody da tabela de serviços no carrinho
    auxProd,    // Variável auxiliar para montar a tabela com os produtos
    auxServ,    // Variável auxiliar para montar a tabela com os serviços
    objProd = [],    // Vetor que irá armazenar todos os produtos
    objServ = [],    // Vetor que irá armazenar todos os serviços
    qtdProd = [],    // Vetor com as quantidades de cada produto
    qtdeServ = [],  // Vetor com as quantidades de cada serviço
    totalCarrinho,  // Preço total com todos produtos e serviços do carrinho
    ids = [],   // Vetor que irá armazenar os ids do produtos no carrinho
    erroModal,  // Modal de mensagens de erro
    erroModalContent,   // Conteúdo do modal de erro
    dialogoModal,   // Modal de mensagens de diálogo
    dialogoModalContent;    // Conteúdo do modal de diálogo

function load() {
    erroModal = document.getElementById('erroModal');
    erroModalContent = document.getElementById('erroModalContent');
    dialogoModal = document.getElementById('dialogoModal');
    dialogoModalContent = document.getElementById('dialogoModalContent');
    if(localStorage.getItem('qtdeProd') == undefined) {
        localStorage.setItem('qtdeProd', 0);
    }
    montaCarrinho();
    ids = JSON.parse(localStorage.getItem('ids'));
    buscaProd.addEventListener('keyup', function(e) {
        localStorage.setItem('busca', buscaProd.value);
        if(e.keyCode == 13) {
            location.href = '/';
        }
    });
}

function montaCarrinho() {
    localStorage.setItem('qtdeServ', 0); // temporariamente o sistema não possui serviços
    objProd = [];
    qtdProd = [];
    for (var i = 0; i <= localStorage.getItem('lastProd'); i++) {
        if(localStorage.getItem('produtos'+i) != undefined) {
            objProd.push(JSON.parse(localStorage.getItem('produtos'+i)));
            qtdProd.push(localStorage.getItem('produtos'+i+'qtde'));
        }
    }
    prodCarrinho = document.querySelector('#prodCarrinho');
    servCarrinho = document.querySelector('#servCarrinho');
    auxProd = "";
    auxServ = "";
    totalCarrinho = 0;
    for (var i = 0; i < objProd.length; i++) {
        auxProd += '<tr id="produto'+i+'"><td><img src="'
        auxProd += objProd[i].imagem == null ? './imagens/produto-sem-imagem.png' : (APInodeJs + '/api/public/produto/img?src=' + objProd[i].imagem)
        auxProd += '"><div class="infoItem"><h1>'+ objProd[i].nome +'</h1><h2>'+ objProd[i].descricao +'</h2><p id="avisoQtd'+i+'" class="aviso"></p></div></td><td><input type="number" id="qtdItem'+ i +'" size="4" maxlength="4" onclick="calculaTotalProd('+i+','+objProd[i].preco+')" onblur="calculaTotalProd('+i+','+objProd[i].preco+')" onkeyup="calculaTotalProd('+i+','+objProd[i].preco+')" value="'+qtdProd[i]+'" min="1" pattern="^[0-9]+"/></td><td>R$'+toMoney(objProd[i].preco)+'</td><td id="precoItem'+i+'">R$'+(toMoney(objProd[i].preco*qtdProd[i]))+'</td><td><button class="btnRemover" onclick="rmvCarrinho('+i+')" title="Remover Item"><i class="material-icons">delete</i></button></td></tr>';
        totalCarrinho += objProd[i].preco*qtdProd[i];
    }
    for (var i = 1; i <= localStorage.getItem('qtdeServ'); i++) {
        objServ = JSON.parse(localStorage.getItem('servicos'+i));
        var qtd = localStorage.getItem('servicos'+i+'qtde');
        auxServ += '<tr><td><img src="'
        auxServ += objServ.imagem == null ? './imagens/produto-sem-imagem.png' : (APInodeJs + '/api/public/produto/img?src=' + objServ.imagem)
        auxServ += '"><div class="infoItem"><h1>' + objServ.nome + '</h1><h2>' + objServ.descricao + '</h2></div></td><td>' + objServ.data + '</td><td><input type="number" id="qtdItem' + i + '" value="' + qtd + '" min="1" pattern="^[0-9]+"/></td><td>R$' + toMoney(objServ.preco) + '</td><td id="precoItem' + i + '">R$' + toMoney(objServ.preco * qtd) + '</td><td><button class="btnRemover" onclick="rmvCarrinho(' + i + ')" title="Remover Item"><i class="material-icons">delete</i></button></td></tr>';
        totalCarrinho += objServ.preco*qtd;
    }
    document.querySelector('#totalCarrinho').innerHTML = 'TOTAL: R$'+ toMoney(totalCarrinho);
    if(localStorage.getItem('qtdeProd') == 0 || localStorage.getItem('qtdeProd') == undefined){
        auxProd = '<tr><td colspan="5">Nenhum produto adicionado ao carrinho.</td></tr>';
    }
    if(localStorage.getItem('qtdeServ') == 0 || localStorage.getItem('qtdeServ') == undefined){
        auxServ = '<tr><td colspan="5">Nenhum serviço adicionado ao carrinho.</td></tr>';
    }
    prodCarrinho.innerHTML = auxProd;
    servCarrinho.innerHTML = auxServ;
    if (localStorage.qtdeProd > 0) {
        document.querySelector('#contCarrinho').innerHTML = localStorage.qtdeProd;
    }
    else {
        document.querySelector('#contCarrinho').innerHTML = 0;
    }
}
function rmvCarrinho(indice) {
    objProd.splice(indice, 1);
    ids.splice(indice, 1);
    localStorage.removeItem('produtos'+indice);
    localStorage.removeItem('produtos'+indice+'qtde');
    for (var i = indice; i < localStorage.getItem('lastProd'); i++) {
        localStorage.setItem('produtos'+i, localStorage.getItem('produtos'+(i+1)));
        localStorage.setItem("produtos" + i + "qtde", localStorage.getItem("produtos" + (i + 1) + "qtde"));
    }
    localStorage.removeItem('produtos' + localStorage.getItem('lastProd'));
    localStorage.removeItem('produtos' + localStorage.getItem('lastProd') + 'qtde');
    localStorage.setItem('ids', JSON.stringify(ids));
    localStorage.setItem('qtdeProd', localStorage.getItem('qtdeProd') - 1);
    if(localStorage.getItem('qtdeProd') == 0) {
        localStorage.removeItem('qtdeProd');
        localStorage.removeItem('lastProd');
    }
    if(localStorage.getItem('lastProd') > 0){
        localStorage.setItem('lastProd', localStorage.getItem('lastProd')-1);
    }
    montaCarrinho();
}
function clearCarrinho() {
    objProd = [];
    qtdProd = [];
    ids = [];
    for (var i = 0; i <= localStorage.getItem('lastProd'); i++) {
        if (localStorage.getItem("produtos" + i) != undefined) {
            localStorage.removeItem("produtos" + i);
            localStorage.removeItem("produtos" + i + "qtde");
        }
    }
    localStorage.removeItem('ids');
    localStorage.removeItem('busca');
    localStorage.removeItem('lastProd');
    localStorage.removeItem('qtdeProd');
    attContCarrinho();
}
function calculaTotalProd(prod, preco) {
    var qtde = document.getElementById("qtdItem" + prod);
    var obj = JSON.parse(localStorage.getItem("produtos" + prod));
    if (qtde.value >= obj.quantidade) {
        qtde.value = obj.quantidade;
        document.getElementById("avisoQtd" + prod).innerHTML = "Estoque disponível: " + obj.quantidade;
    }
    if (qtde.value.indexOf("-") != -1) {
        qtde.value = qtde.value.replace("-", "");
    }
    if (qtde.value == 0) {
        qtde.value = 1;
    }
    document.getElementById("precoItem" + prod).innerHTML = 'R$' + toMoney(qtde.value * preco);
    calculaTotalCarrinho();
}
function calculaTotalCarrinho() {
    totalCarrinho = 0;
    for (var i = 0; i <= localStorage.getItem('lastProd'); i++) {
        if (localStorage.getItem('produtos' + i)) {
            var qtde = document.getElementById('qtdItem' + i);
            var obj = JSON.parse(localStorage.getItem('produtos' + i));
            var precoItem = qtde.value * obj.preco;
            totalCarrinho += precoItem;
        }
    }
    document.querySelector('#totalCarrinho').innerHTML = 'TOTAL: R$' + toMoney(totalCarrinho);
}
function sendCarrinho() {
    erroModal.classList.remove("centro");
    dialogoModal.classList.remove("centro");
    if (localStorage.token != undefined) {
        if (localStorage.getItem('qtdeProd') > 0) {
            var auxCarrinho = [];
            for (var i = 0; i < localStorage.getItem('qtdeProd'); i++) {
                auxCarrinho.push({
                    "qtde": parseInt(document.getElementById('qtdItem' + i).value),
                    "preco": objProd[i].preco,
                    "id": objProd[i].id
                });
            }
            var route = APInodeJs + '/api/private/pedido',
                data = {
                "produtos": auxCarrinho,
                "servicos": []
            };
            document.querySelector('.loop').style.display = "block";
            document.querySelector('.loader').style.display = "block";
            setRequestCarrinho(sendCarrinhoEstado, route, data);
        } else {
            document.getElementById('erroModalContent').innerHTML = '<h2>O carrinho está vazio</h2><p>É necessário adicionar itens ao carrinho para registrar um pedido</p><button class="btn btn-text" id="btnFecha" onclick="closeModalErro()">FECHAR</button>';
            document.getElementById('erroModal').className = 'background-modal centro';
        }
    } else {
        showLogin();
    }
}
function sendCarrinhoEstado() {
    if(this.readyState == READY_STATE_COMPLETE) {
        document.querySelector('.loop').style.display = "none";
        document.querySelector('.loader').style.display = "none";
        if(this.status == 201) {
            clearCarrinho();
            if(window.innerWidth > 600){
                showNotif("Obrigado por registrar seu pedido! Em breve nossa equipe entrará em contato com você.", true);
            }
            else {
                showNotif("Obrigado por registrar seu pedido!", true);
            }
            setTimeout(index,5000);
        }
        else if (this.status == ACESS_DENIED) {
            var json_data = JSON.parse(this.responseText);
            erroModalContent.innerHTML = '<h1>Erro - '+ this.status +'</h1><h2>'+ json_data.message +'</h2><p>Tente novamente mais tarde</p><button class="btn btn-contained" style="margin-bottom:10px" id="btnFecha" onclick="closeModalErro()">FECHAR</button>';
            erroModal.className = 'background-modal centro';
            if(localStorage.token != undefined){
                localStorage.removeItem('nome');
                localStorage.removeItem('token');
                setTimeout(showLogin, 5000);
            }
        }
        else {
            var json_data = JSON.parse(this.responseText);
            erroModalContent.innerHTML = '<h1>Erro - '+ this.status +'</h1><h2>'+ json_data.message +'</h2><p>Tente novamente mais tarde</p><button class="btn btn-contained" id="btnFecha" onclick="closeModalErro()">FECHAR</button>';
            erroModal.className = 'background-modal centro';
            clearCarrinho();
        }
    }
}
function index() {
    location.href = '/';
}
var setRequestCarrinho = (callback, route, data) => {   // (callback = função que irá interagir com a resposta recebida pelo servidor, route = a direção por onde será feita a requisição, data = dados do tipo JSON que será a informação que será enviada para o servidor)
    var req = meuNavegador(); // Receberá new XMLHttpRequest() ou new ActiveXObject dependendo do navegador
    data = JSON.stringify(data);
    req.onreadystatechange = callback;  // Método onreadystatechange é chamado toda vez que o estado da requisição for alterado
    req.open('POST', route, true); // open = método que irá abrir a conexão com o servidor ; (method = método http pelo qual será enviado a informação, route = rota por onde vai ser enviada a informação, boolean = determina se a requisição vai ser assíncrona ou sincrona)
    req.setRequestHeader('Content-Type','application/json');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send(data);
}
function confirmCarrinho() {
    if (localStorage.token != undefined) {
        dialogoModalContent.innerHTML = '<p>Deseja registrar seu pedido?</p><div class="alinha-final"><button class="btn btn-text" id="btnFecha" onclick="closeModalErro()">FECHAR</button><button class="btn btn-text" onclick="sendCarrinho()">CONFIRMAR</button></div>';
        dialogoModal.className = "background-modal centro";
    }
    else {
        showLogin();
    }
}