var NOT_FOUND = 404,    // Código de estado http
    itensPagina,    // Variável que recebe a quantidade de itens na tabela
    pag,    // Variável que recebe a página atual da tabela
    totalPedidos,   // Variável que recebe a quantidade total de pedidos
    totalPaginas;   // Variável que recebe a quantidade total de páginas da tabela

/* COMANDOS QUE SERÃO EXECUTADOS AO CARREGAR A PÁGINA */
function load() {
    totalPaginas = document.querySelector('#totalPaginas');
    itensPagina = document.querySelector('#itensPagina');
    pag, totalPedidos;
    getTabela(1);
}

/* FUNÇÕES DA TABELA */
function prevPgTab() {
    if (pag > 1) {
        getTabela(pag - 1);
    }
}
function nextPgTab() {
    if (pag < Math.ceil(totalFunc / 10)) {
        getTabela(pag + 1);
    }
}
function getTabela(pagina){
    var route = `${APInodeJs}/api/private/pedidocliente?pagina=${pagina}`;

    // var route = `http://localhost:3002/api/private/pedidocliente?pagina=${pagina}`;

    pag = pagina;
    setRequestGetAuth(estadoTabela, route);
}
function estadoTabela(){
    if(this.readyState == READY_STATE_COMPLETE){
        if(this.status == ACESS_DENIED){
            var json_data = JSON.parse(this.responseText);
            if(json_data.message == "Sessão expirada"){
                logout();
            }
            location.href = '/';
        } else if(this.status == STATUS_OK){
            var json_data = JSON.parse(this.responseText);
            var listaPedidos = '<table class="tabela"><thead><tr><th>Nº DO PEDIDO</th><th>DATA/HORA</th><th>PREÇO TOTAL</th><th>STATUS</th></tr></thead><tbody>';
            for(var i = 0; i < json_data.dados.pedidos.length; i++){
                var data = new Date(json_data.dados.pedidos[i].dataabertura).toLocaleString();
                data = data.substr(0, 11) + "- " + data.substr(11, 5);
                listaPedidos += "<tr onclick='detalhesPedido(" + json_data.dados.pedidos[i].id + ")'><td>" + json_data.dados.pedidos[i].id + "</td><td>" + data + "</td><td>R$ " + toMoney(json_data.dados.pedidos[i].precoproduto + json_data.dados.pedidos[i].precoservico) + "</td><td>" + json_data.dados.pedidos[i].status + "</td></tr>";
            }
            listaPedidos += '</tbody></table>';
            document.getElementById("base").innerHTML = listaPedidos;
            document.getElementById("paginacao").style.display = "flex";
            document.getElementById('loading').style.display = "none";
            totalFunc = json_data.dados.total;
            totalPaginas.innerHTML = "de " + totalFunc;
            if (pag >= Math.ceil(totalFunc / 10)) {
                document.querySelector('#btnProx').style.opacity = "0.3";
                document.querySelector('#btnProx').style.cursor = "not-allowed";
                itensPagina.innerHTML = 1 + (pag - 1) * 10 + " - " + totalFunc;
            }
            else {
                document.querySelector('#btnProx').style.opacity = "1";
                document.querySelector('#btnProx').style.cursor = "pointer";
                itensPagina.innerHTML = 1 + (pag - 1) * 10 + " - " + pag * 10 + " ";
            }
            if (pag == 1) {
                document.querySelector('#btnVoltar').style.opacity = "0.3";
                document.querySelector('#btnVoltar').style.cursor = "not-allowed";
            }
            else {
                document.querySelector('#btnVoltar').style.opacity = "1";
                document.querySelector('#btnVoltar').style.cursor = "pointer";
            }
        } else if(this.status == NOT_FOUND){
            var json_data = JSON.parse(this.responseText);
            document.querySelector(".divMeusPedidos").style.display = "none";
            document.getElementById('loading').style.display = "none";
            if(json_data.message == "Nenhum pedido encontrado"){
                document.getElementById("base").innerHTML = `<div class="nenhumPedido"><h2>Ops! Que pena :( <br>Você ainda não realizou nenhum pedido.<br>Faça já o seu pedido!<br><br><a href="${webCliente}">${webCliente}</a></h2></div>`;
            }
        }
    }
}

function detalhesPedido(idPedido){
    location.href = "pedido?pedido=" + idPedido;
}