// Depois que carregar a página efetua o que estiver dentro para que ache antes da tag Body
function load () {
    // Declarando variaveis para bloqueio de Copiar campo
    var inputEmail = document.getElementById('email');
    var inputConfEmail = document.getElementById('confirmae');
    var inputSenha = document.getElementById('senha');
    var inputConfSenha = document.getElementById('confirmas');
    var pegaddd = document.querySelector('#ddd');
    var peganome = document.querySelector("#nome");
    var peganumero = document.getElementById("numero");
    // Mascara numero para telefone
    // Mascara Jquery para o DDD
    $('#ddd').mask('(00)');
    peganumero.onblur = function () {
        if (peganumero.value.length < 9 && (peganumero.value.length != '')) {
            document.querySelector('.msgT').innerHTML = "<p style='color: red; font-size: 16px;'>Numero inválido</p>";
        } else {
            document.querySelector('.msgT').innerHTML = "";
        }
    }
    peganumero.maxLength = 10;

    var mascaraNumero = function (val) {
        return val.replace(/\D/g, '').length === 9 ? '00000-0000' : '0000-00009';
    },
        options = {
            onKeyPress: function (val, e, campo, options) {
                campo.mask(mascaraNumero.apply({}, arguments), options);
            }
        };

    $('#numero').mask(mascaraNumero, options);
    inputEmail.onpaste = function (e) {
        e.preventDefault(); //não deixa copiar e colar no campo
    }
    inputEmail.onblur = function (e) {
        verificaCE();
        var email = new RegExp(/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}$/);
        if (email.test(inputEmail.value.toLowerCase())) {
            document.querySelector('.msgEmail').innerHTML = "";
        } else {
            document.querySelector('.msgEmail').innerHTML = "<p style='color: red;'> E-mail inválido </p>";
        }
    }
    inputConfEmail.onblur = function () { verificaCE(); }
    inputSenha.onblur = function () { verificaCS(); }
    inputConfSenha.onblur = function () { verificaCS(); }
    inputConfEmail.onpaste = function (e) {
        e.preventDefault();
    }
    inputSenha.onpaste = function (e) {
        e.preventDefault();
    }
    inputConfSenha.onpaste = function (e) {
        e.preventDefault();
    }
    pegaddd.onkeydown = function (evt) {
        var code = (window.event) ? window.event.keyCode : evt.which;
        if ((code > 57 || (code < 48 && code != 8)) && (code != 37 && code != 39 && code != 46) && (code != 16) && (code != 9) && !(code >= 96 && code <= 105)) { //verificando se é letra ou numero verificar o keycode das setas
            return false; // se for letra irá retornar false e não deixara digitar
        }
    }
    pegaddd.onkeyup = function (e) {
        var numero = new RegExp(/\d/);

        if (!numero.test(e.key) && (e.which != 9 && e.which != 16 && e.which != 8 && e.which != 46)) {
            e.preventDefault();
        }
        if (pegaddd.value.length >= 3 && (e.which != 9 && e.which != 16 && e.which != 8 && e.which != 46) && numero.test(e.key)) {
            document.querySelector('#numero').focus(); //se ja for digitado a quantidade necessária ele pula para o input do numero
        }
    }
    pegaddd.onblur = function () {
        if (pegaddd.value.length != 4 && pegaddd.value != "") {
            document.querySelector('.msgD').innerHTML = "<p style='color: red; font-size: 16px;'>DDD inválido</p>";
        } else {
            document.querySelector('.msgD').innerHTML = "";
        }
    }
    function verificaCS() {
        var s = document.getElementById('senha');
        var cs = document.getElementById('confirmas');
        if (s.value != cs.value && s.value != "" && cs.value != "") {
            document.querySelector('.msgS').innerHTML = "<p style='color: red; font-size: 16px;'>Senha diferente</p>";
        }
        else {
            document.querySelector('.msgS').innerHTML = "";
        }
        if (s.value.length < 6 || (s.value.length > 20)) {
            document.querySelector('.msgAviso').innerHTML = "<p style='color: red; font-size: 16px;'>Senha deve conter de 6 à 20 caracteres</p>";
        }
        else {
            document.querySelector('.msgAviso').innerHTML = "";
        }
    }
}
function verificaCE() {
    var e = document.getElementById('email');
    var ce = document.getElementById('confirmae');
    if (e.value.toLowerCase() != ce.value.toLowerCase() && ce.value.toLowerCase() != "") {
        document.querySelector('.msgE').innerHTML = "<p style='color: red; font-size: 16px;'>E-mail diferente</p>";
    }
    if (e.value.toLowerCase() == ce.value.toLowerCase()) {
        document.querySelector('.msgE').innerHTML = "";
    }
}
// Validando campos input
function validacao() {
    var inputNome = document.querySelector("#nome");
    var pegaddd = document.querySelector('#ddd');
    var s = document.getElementById('senha');
    var cs = document.getElementById('confirmas');
    var e = document.getElementById('email');
    var ce = document.getElementById('confirmae');
    if (inputNome.value.replace(/^\s+|\s+$/g, '').length == 0) {
        document.querySelector('.msgN').innerHTML = "<p style='color: red; font-size: 16px;'>Não é permitido espaço na primeira posição do nome!</p>"
    }
    if (e.value.toLowerCase() != ce.value.toLowerCase()) {
        document.querySelector('.msgE').innerHTML = "<p style='color: red; font-size: 16px;'>E-mail diferente</p>";
    }
    if (e.value.toLowerCase() == ce.value.toLowerCase()) {
        document.querySelector('.msgE').innerHTML = "";
    }
    if (s.value != cs.value) {
        document.querySelector('.msgS').innerHTML = "<p style='color: red; font-size: 16px;'>Senha diferente</p>";
    }
    if (s.value == cs.value) {
        document.querySelector('.msgS').innerHTML = "";
    }
    if (pegaddd.value.length != 4) {
        document.querySelector('.msgD').innerHTML = "<p style='color: red; font-size: 16px;'>DDD inválido</p>";
    }
    if (pegaddd.value.length == 4) {
        document.querySelector('.msgD').innerHTML = "";
    }
    var email = new RegExp(/^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/);
    if (!email.test(e.value.toLowerCase())) {
        document.querySelector('.msgEmail').innerHTML = "<p style='color: red;'> E-mail inválido </p>";
    }
    if ((e.value.toLowerCase() == ce.value.toLowerCase()) && (s.value == cs.value) && (pegaddd.value.length == 4) && (email.test(e.value.toLowerCase())) && (inputNome.value[0] != " ")) {
        document.querySelector('.msgE').innerHTML = "";
        document.querySelector('.msgS').innerHTML = "";
        document.querySelector('.msgD').innerHTML = "";
        document.querySelector('.msgT').innerHTML = "";
        document.querySelector('.msgEmail').innerHTML = "";
        sendCadastro();
    }
    return false;
}
// Enviando Json para API
function sendCadastro() {
    var route = APIdotNet +'/api/public/cliente',
        data = {
            'nome': capitalize(document.getElementById("nome").value),
            'email': document.getElementById("email").value.toLowerCase(),
            'telefone': document.getElementById("ddd").value + document.getElementById("numero").value,
            'senha': document.getElementById("senha").value
        }
    data.telefone = data.telefone.replace('(', '').replace(')', '').replace('-', ''); //retirando a mascara para enviar para o banco
    setRequestPost(estado, route, data);
    document.querySelector('.loop').style.display = "block";
    document.querySelector('.loader').style.display = "block";
    return false;
}
function fechaModal() {
    document.querySelector('.background-modal').style.display = "none";
}
function estado() {
    if (this.readyState == 4) {
        if (this.status == 200) {
            var json_data = JSON.parse(this.responseText);
            localStorage.setItem('nome', json_data.usuario.nome);
            localStorage.setItem('token', json_data.token);
            window.location.assign(webCliente);
        }
        else if (this.status == 201) {
            document.querySelector('.msgE').innerHTML = "";
            document.querySelector('.msgT').innerHTML = "";
            var route = APIseguranca +'/api/public/cliente/login',
                data = {
                    'email': document.getElementById("email").value.toLowerCase(),
                    'senha': document.getElementById("senha").value
                }
            setRequestPost(estado, route, data);
        }
    }
    else if (this.status == 400) {
        var json_data = JSON.stringify(this.responseText);
        var nome = new RegExp(/nome/)
        var email = new RegExp(/email/);
        var senha = new RegExp(/senha/);
        var telefone = new RegExp(/telefone/);
        var emailD = new RegExp(/email duplicado/);
        var telefoneD = new RegExp(/telefone duplicado/);
        if (emailD.test(json_data)) {
            document.querySelector('.msgEmail').innerHTML = "<p style='color: red; font-size: 16px;'>E-mail já cadastrado</p>";
        } else if (email.test(json_data)) {
            document.querySelector('.msgE').innerHTML = "<p style='color: red; font-size: 16px;'>E-mail inválido</p>";
        }
        if (telefoneD.test(json_data)) {
            document.querySelector('.msgT').innerHTML = "<p style='color: red; font-size: 16px;'>Telefone já cadastrado</p>";
        } else if (telefone.test(json_data)) {
            document.querySelector('.msgT').innerHTML = "<p style='color: red; font-size: 16px;'>Telefone inválido</p>";
        }
        if (nome.test(json_data)) {
            document.querySelector('.msgN').innerHTML = "<p style='color: red; font-size: 16px;'>Nome obrigatório</p>";
        }
        if (senha.test(json_data)) {
            document.querySelector('.msgS').innerHTML = "<p style='color: red; font-size: 16px;'>Senha inválida</p>";
        }
        document.querySelector('.loop').style.display = "none";
    }
    // 500 err server
    else if (this.status == 500) {
        document.querySelector('.loop').style.display = "none";
        document.querySelector('.loader').style.display = "none";
        document.querySelector('.background-modal').style.display = "block";
    }
}
function redirecionar() {
    window.location.assign(webCliente);
}