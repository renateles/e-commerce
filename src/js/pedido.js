var cancelarModal, selectMotivos;

/* COMANDOS QUE SERÃO EXECUTADOS AO CARREGAR A PÁGINA */
function load() {
    const idPedido = parseInt(location.href.split("=")[1]);
    cancelarModal = document.querySelector('#cancelarModal');
    selectMotivos = document.querySelector('#selectMotivos');
    document.getElementById("voltarCancelar").addEventListener("click", function () {
        cancelarModal.style.display = 'none';
    });

    window.onkeydown = (event) => {
        if (event.keyCode === 27) {
            cancelarModal.style.display = 'none';
        }
    }
    getInformacoesPedido(idPedido);
}

window.onclick = function (event) {
    if (event.target == cancelarModal) {
        cancelarModal.style.display = 'none';
    }
}

function getInformacoesPedido(idPedido) {
    if (idPedido != NaN && idPedido != undefined) {
        var route = `${APInodeJs}/api/private/pedidocliente/${idPedido}`;
        // var route = `http://localhost:3002/api/private/pedidocliente/${idPedido}`;
        setRequestGet(estadoTabela, route);
    } else {
        location.href = 'index.html';
    }
}

setRequestGet = (callback, route) => {
    var req = new XMLHttpRequest();
    req.onreadystatechange = callback;
    req.open('GET', route, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send();
}

function estadoTabela() {
    if (this.readyState == READY_STATE_COMPLETE) {
        if (this.status == ACESS_DENIED) {
            var json_data = JSON.parse(this.responseText);
            if (json_data.message == "Sessão expirada") {
                logout();
            }
            location.href = 'index.html';
        } else if (this.status == STATUS_OK) {
            document.getElementById("infoPedido").style.display = "block";
            var json_data = JSON.parse(this.responseText);
            var totalCarrinho = 0;
            document.getElementById("numeroPedido").innerText = json_data.dados.id;
            document.getElementById("status").innerText = json_data.dados.status;
            document.getElementById('loading').style.display = "none";

            var info = document.getElementById("info");
            if (json_data.dados.status == "Registrado") {
                var dataLimiteCancelar = new Date(json_data.dados.dataabertura);
                dataLimiteCancelar.setDate(dataLimiteCancelar.getDate() + 3);
                var dataAtual = new Date();
                if (dataLimiteCancelar > dataAtual) {
                    info.innerHTML = `<button onclick="showCancelar(${json_data.dados.id})" class="btn btn-text">Cancelar Pedido</button>`;
                }
            } else if (json_data.dados.status == "Cancelado") {
                var dataCancelado = new Date(json_data.dados.datafechamento).toLocaleString();
                dataCancelado = dataCancelado.substr(0, 11);
                info.innerHTML = `<b>Cancelado em: ${dataCancelado}</b>`;
                document.getElementById("divMotivoCancelamento").style.display = "block";
                document.getElementById("motivoCancelamento").innerHTML = `${json_data.dados.motivocancelamento}`;
                document.getElementById("descricaoCancelamento").innerHTML = `${json_data.dados.observacao != null ? json_data.dados.observacao : ""}`;
            } else if (json_data.dados.status == "Finalizado") {
                var dataFinalizado = new Date(json_data.dados.datafechamento).toLocaleString();
                dataFinalizado = dataFinalizado.substr(0, 11);
                info.innerHTML = `<b>Finalizado em: ${dataFinalizado}</b>`;
            }

            if (json_data.dados.produtos != null) {
                var listaProdutos = `<table class="tabela">
                                        <thead>
                                            <tr>
                                                <th>PRODUTO</th>
                                                <th>QUANTIDADE</th>
                                                <th>PREÇO UN.</th>
                                                <th>PREÇO TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody>`;
                for (var i = 0; i < json_data.dados.produtos.length; i++) {
                    var prod = json_data.dados.produtos[i];
                    totalCarrinho += prod.preco * prod.quantidade;
                    listaProdutos += `<tr>
                                        <td>
                                            <img src="${prod.imagem == null ? './imagens/produto-sem-imagem.png' : APInodeJs + '/api/public/produto/img?src=' + prod.imagem}">
                                            <div class="infoItem">
                                                <h1>${prod.nome}</h1>
                                                <h2>${prod.descricao}</h2>
                                            </div>
                                        </td>
                                        <td>
                                            ${prod.quantidade}
                                        </td>
                                        <td>
                                            R$${toMoney(prod.preco)}
                                        </td>
                                        <td>
                                            R$${toMoney(prod.preco * prod.quantidade)}
                                        </td>
                                    </tr>`;
                }
                listaProdutos += `</tbody></table>`;
                document.getElementById("baseTabelaProduto").innerHTML = listaProdutos;
            }
            if (json_data.dados.servicos != null) {
                var listaServicos = `<table class="tabela">
                                        <thead>
                                            <tr>
                                                <th>SERVIÇO</th>
                                                <th>DATA</th>
                                                <th>QTD/HORAS</th>
                                                <th>PREÇO/HORA</th>
                                                <th>PREÇO TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody>`;
                for (var i = 0; i < json_data.dados.servicos.length; i++) {
                    var servico = json_data.dados.servicos[i];
                    var data = new Date(servico.datainicio).toLocaleString();
                    data = data.substr(0, 11);
                    totalCarrinho += servico.horas * servico.preco;
                    listaServicos += `<tr>
                                        <td>
                                            <img src="./imagens/produto-sem-imagem.png">
                                            <div class="infoItem">
                                                <h1>${servico.nome}</h1>
                                                <h2>${servico.descricao}</h2>
                                            </div>
                                        </td>
                                        <td>
                                            ${data}
                                        </td>
                                        <td>
                                            ${servico.horas}
                                        </td>
                                        <td>
                                            R$${toMoney(servico.preco)}
                                        </td>
                                        <td>
                                            R$${toMoney(servico.horas * servico.preco)}
                                        </td>
                                    </tr>`;
                }
                listaServicos += `</tbody></table>`;
                document.getElementById("baseTabelaServico").innerHTML = listaServicos;
            }
            document.getElementById("baseTabelaTotal").innerHTML = `<table class="tabela">
                <td style="text-align: right" colspan="5">
                    VALOR TOTAL DO PEDIDO: R$${toMoney(totalCarrinho)}
                </td>
            </table>`;

        }
    }
}

function meusPedidos() {
    location.href = "meuspedidos";
}

function showCancelar(id) {
    cancelarModal.style.display = 'flex';
    document.getElementById("btnConfirmarCancelar").setAttribute('onclick', `cancelarPedido(${id})`);
    getMotivoCancelamento();
}

function getMotivoCancelamento() {
    var route = `${APInodeJs}/api/private/motivo`;
    // var route = `http://localhost:3002/api/private/motivo`;
    setRequestGet(estadoMotivoCancelamento, route);
}

var setRequestGet = (callback, route) => {
    var req = new XMLHttpRequest();
    req.onreadystatechange = callback;
    req.open('GET', route, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send();
}

function estadoMotivoCancelamento() {
    if (this.readyState == READY_STATE_COMPLETE) {
        if (this.status == STATUS_OK) {
            var json_data = JSON.parse(this.responseText);
            var campoMotivos = "";
            for (var i = 0; i < json_data.message.length; i++) {
                campoMotivos += `<option value="${json_data.message[i].id}">${json_data.message[i].motivo}</option>`
            }
            selectMotivos.innerHTML = campoMotivos;
        }
    }
}

function cancelarPedido(id) {
    closeNotif();
    var route = `${APInodeJs}/api/private/cancelarpedido`;
    // var route = `http://localhost:3002/api/private/cancelarpedido`;
    data = {
        'idPedido': id,
        'idCancelamento': parseInt(selectMotivos.value),
        'descricao': document.getElementById("descricao").value
    }
    document.querySelector(".loop-cancelar").style.display = "flex";
    setRequest(estadoSendCancelar, route, data);
}

var setRequest = (callback, route, data) => {
    var req = new XMLHttpRequest();
    data = JSON.stringify(data);
    req.onreadystatechange = callback;
    req.open('PUT', route, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send(data);
}

function estadoSendCancelar() {
    if (this.readyState == READY_STATE_COMPLETE) {
        document.querySelector(".loop-cancelar").style.display = "none";
        if (this.status == 200) {
            document.getElementById("info").style.display = "none";
            cancelarModal.style.display = "none";
            showNotif("Seu pedido foi cancelado com sucesso!.", true);
            setTimeout(meusPedidos, 4000);
        }
        else if (this.status == 401) {
            var json_data = JSON.parse(this.responseText);
            if (json_data.message == "Este pedido não pertence a este cliente") {
                showNotif("Este pedido não pertence a este cliente", false);
                setTimeout(meusPedidos, 4000);
            }
        }
    }
}