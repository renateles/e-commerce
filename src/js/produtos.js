var listaProd,  // Local no HTML onde serão mostrados os produtos
    totalProdLista,  // Total de produtos mostrados na página
    totalProd,  // Total de produtos cadastrados
    produtos = [],   // Vetor com os produtos
    prodModal,  // Armazena informações completas do produto selecionado (modal)
    ids = [],   // Vetor que armazena os ids dos produtos adicionados ao carrinho
    prodExp,
    imgDisp,
    itemCarrinho,
    totalPaginas,
    pag,
    btnProx,
    btnVoltar;

function load() {
    if(window.innerWidth < 600){
        document.getElementById('banner').innerHTML = `<img src="src/imagens/banner-bf-min-transp.png">`;
    }
    else {
        document.getElementById('banner').innerHTML = `<img src="src/imagens/banner-bf-transp.png">`;
    }
    prodExp = document.querySelector('#prodModal');
    imgDisp = document.querySelector('#img-disp');
    totalPaginas = document.querySelector('#totalPaginas');
    btnProx = document.querySelector('#btnProx');
    btnVoltar = document.querySelector('#btnVoltar');
    listarProdutos(1);
    attContCarrinho();
    if(localStorage.getItem('ids')){
        ids = JSON.parse(localStorage.getItem('ids'));
    }
    if(localStorage.busca != undefined) {   // Verifica se o usuário estava fazendo uma busca
        buscaProd.value = localStorage.getItem('busca');
        listarProdutos(1, buscaProd.value);
    }
    buscaProd.addEventListener('keyup', function() {    // Armazena e realiza a busca
        localStorage.setItem('busca', buscaProd.value);
        setTimeout(listarProdutos(1, buscaProd.value),1000);
    });
    if (buscaProd.value != undefined) {
        listarProdutos(1, buscaProd.value);
    }
}

/* FUNÇÕES ATIVADAS POR CLICK NA TELA */
window.addEventListener('click', function (event) {
    if (event.target == prodExp) {
        closeModal();
    }
});

window.addEventListener('resize', function() {
    if(window.innerWidth < 600){
        document.getElementById('banner').innerHTML = `<img src="imagens/banner-bf-min-transp.png">`;
    }
    else {
        document.getElementById('banner').innerHTML = `<img src="imagens/banner-bf-transp.png">`;
    }
});
function showListaProd() {  // Função que monta a lista de produtos
    listaProd = document.querySelector('#listaProd');
    var aux = "";
    for (var i = 0; i < totalProdLista; i++) {
        if (produtos[i].quantidade > 0) { // Verifica se o produto está disponível
            aux += `<li class="item" onclick="showProduto(${produtos[i].id})">
                <img src="${produtos[i].imagem == null ? './imagens/produto-sem-imagem.png' : APInodeJs + '/api/public/produto/img?src=' + produtos[i].imagem}">
                <h1>${produtos[i].nome}</h1>
                <p>R$${toMoney(produtos[i].preco)}</p>
            </li>`
        }
        else {  // Produto indisponível
            aux += `<li class="item indisp" onclick="showProduto(${produtos[i].id})">
                <div class="imgIndisp">
                    <img src="${produtos[i].imagem == null ? './imagens/produto-sem-imagem.png' : APInodeJs + '/api/public/produto/img?src=' + produtos[i].imagem}">
                    <p>Produto indisponível</p>
                </div>
                <h1>${produtos[i].nome}</h1>
                <p>R$${toMoney(produtos[i].preco)}</p>
            </li>`
        }
    }
    listaProd.innerHTML = aux;
}
function showProduto(id) {
    var route = `${APIdotNet}/api/public/produto/${id}`;
    setRequestGet(estadoProdCompleto, route);
}
function montaModalProduto() {
    if (prodModal.quantidade > 0) { // Verifica se o produto está disponível
        prodExp.innerHTML = `<div class="modal-produto">
                <img src="${prodModal.imagem == null ? './imagens/produto-sem-imagem.png' : APInodeJs + '/api/public/produto/img?src=' + prodModal.imagem}">
                <div class="info-produto">
                    <h1>${prodModal.nome}</h1>
                    <h2>${prodModal.descricao}</h2>
                    <p class="precoUnid" id="precoUnidProd">R$${toMoney(prodModal.preco)}</p>
                    <p class="qtdeProd">Quantidade:<input type="number" id="qtdeProdInput" value="1" min="1" pattern="^[0-9]+"></p>
                    <p id="avisoQtd" class="aviso"></p>
                    <div class="addCarrinho alinha-espacado">
                        <div class="container-preco">
                            <p class="precoTotal">TOTAL:</p>
                            <p id="precoTotalProd">R$${toMoney(prodModal.preco)}</p>
                        </div>
                        <button class="btn btn-contained btnCarrinho centro max" onclick="addCarrinho()">Adicionar ao carrinho</button>
                        <button class="btn btn-contained btnCarrinho centro min" onclick="addCarrinho()"><i class="material-icons">add_shopping_cart</i></button>
                    </div>
                </div>
                <button class="btnFechar" onclick="closeModal()">&times;</button>
            </div>`
        prodExp.className = "background-modal centro";
    }
    else {  // Se o produto estiver indisponível
        prodExp.innerHTML = `<div class="modal-produto">
        <img src="${prodModal.imagem == null ? './imagens/produto-sem-imagem.png' : APInodeJs + '/api/public/produto/img?src=' + prodModal.imagem}" style="opacity: 0.6">
                <div class="info-produto">
                    <h1>${prodModal.nome}</h1>
                    <h2>${prodModal.descricao}</h2>
                    <p class="precoUnid" id="precoUnidProd" style="color: black">R$${toMoney(prodModal.preco)}</p>
                    <p class="qtdeProd"><input type="text" disabled id="qtdeProdInput" value="Produto indisponível" style="font-size: 14px; width: 160px"/></p>
                    <div class="addCarrinho alinha-espacado">
                        <div class="container-preco">
                            <p class="precoTotal">TOTAL:</p>
                            <p id="precoTotalProd">R$0,00</p>
                        </div>
                        <button class="btn btnCarrinho centro max" title="Este produto encontra-se indisponível no momento" style="cursor: not-allowed; background-color: silver;">Adicionar ao carrinho</button>
                        <button class="btn btnCarrinho centro min" title="Este produto encontra-se indisponível no momento" style="cursor: not-allowed; background-color: silver;"><i class="material-icons">add_shopping_cart</i></button>
                    </div>
                </div>
                <button class="btnFechar" onclick="closeModal()">&times;</button>
            </div>`
        prodExp.className = "background-modal centro";
    }
    var qtdeProdInput = document.querySelector('#qtdeProdInput'),
        precoTotalProd = document.querySelector('#precoTotalProd'),
        precoUnidProd = document.querySelector('#precoUnidProd');
    qtdeProdInput.select();
    qtdeProdInput.onclick = function() {
        if(qtdeProdInput.value >= prodModal.quantidade){
            qtdeProdInput.value = prodModal.quantidade;
            document.getElementById('avisoQtd').innerHTML = `Estoque disponível: ${prodModal.quantidade}`;
        }
        precoTotalProd.innerHTML = `R$${toMoney((prodModal.preco * qtdeProdInput.value))}`;
    }
    qtdeProdInput.onkeyup = function() {
        if(qtdeProdInput.value >= prodModal.quantidade){
            qtdeProdInput.value = prodModal.quantidade;
            document.getElementById('avisoQtd').innerHTML = `Estoque disponível: ${prodModal.quantidade}`;
        }
        if (qtdeProdInput.value.indexOf("-") != -1) {
            qtdeProdInput.value = qtdeProdInput.value.replace("-", "");
        }
        if (qtdeProdInput.value == 0) {
            qtdeProdInput.value = 1;
        }
        precoTotalProd.innerHTML = `R$${toMoney((prodModal.preco * qtdeProdInput.value))}`;
    }
}
function listarProdutos(pagina, nome) {
    if (nome != null) {
        var route = `https://e-commercesmndotnet.azurewebsites.net/api/public/produto?pagina=${pagina}&nome=${nome}`;
    }
    else {
        var route = `https://e-commercesmndotnet.azurewebsites.net/api/public/produto?pagina=${pagina}&nome=`;
    }
    pag = pagina;
    setRequestGet(estadoProd, route);
}
function estadoProd() {
    if (this.readyState == READY_STATE_COMPLETE) {
        if (this.status == STATUS_OK) {
            produtos = [];
            var json_data = JSON.parse(this.responseText);
            for (var i = 0; i < json_data.produtos.length; i++) {
                produtos.push(json_data.produtos[i]);
            }
            totalProdLista = json_data.produtos.length;
            showListaProd();
            totalProd = json_data.total;
            totalPaginas.innerHTML = "de " + totalProd;
            if (pag >= Math.ceil(totalProd / 20)) {
                btnProx.style.opacity = "0.3";
                btnProx.style.cursor = "not-allowed";
                itensPagina.innerHTML = `${1 + (pag - 1) * 20} - ${totalProd}`
            }
            else {
                btnProx.style.opacity = "1";
                btnProx.style.cursor = "pointer";
                itensPagina.innerHTML = `${1 + (pag - 1) * 20} - ${pag * 20} `
            }
            if (pag == 1) {
                btnVoltar.style.opacity = "0.3";
                btnVoltar.style.cursor = "not-allowed";
            }
            else {
                btnVoltar.style.opacity = "1";
                btnVoltar.style.cursor = "pointer";
            }
            if (json_data.produtos.length == 0) {
                listaProd.innerHTML = `<p style="text-align: center">Produto não encontrado...</p>`;
                itensPagina.innerHTML = `0 - 0`
            }
        }
    }
}
function estadoProdCompleto() {
    if (this.readyState == READY_STATE_COMPLETE) {
        if (this.status == STATUS_OK) {
            var json_data = JSON.parse(this.responseText);
            prodModal = json_data;
            montaModalProduto();
        }
    }
}
var setRequestGet = (callback, route) => {
    var req = meuNavegador();
    req.onreadystatechange = callback;
    req.open('GET', route, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.send();
}
function addCarrinho() {
    if (localStorage.getItem('lastProd') != undefined) {
        itemCarrinho = parseInt(localStorage.getItem('lastProd')) + 1;
    }
    else {
        itemCarrinho = 0;
    }
    if (ids.indexOf(prodModal.id) != -1) {
        var posicao = ids.indexOf(prodModal.id);
        var qtdTotal = parseInt(qtdeProdInput.value) + parseInt(localStorage.getItem(`produtos${posicao}qtde`));
        if (qtdTotal >= ((JSON.parse(localStorage.getItem(`produtos${posicao}`))).quantidade)) {
            qtdTotal = JSON.parse(localStorage.getItem(`produtos${posicao}`)).quantidade;
        }
        localStorage.setItem(`produtos${posicao}qtde`, qtdTotal)
    }
    else {
        ids.push(prodModal.id);
        localStorage.setItem(`ids`, JSON.stringify(ids));
        localStorage.setItem(`produtos${itemCarrinho}`, JSON.stringify(prodModal)); // produto que foi adicionado ao carrinho
        localStorage.setItem(`produtos${itemCarrinho}qtde`, parseInt(qtdeProdInput.value))    // quantidade do produto que foi adicionado ao carrinho
        localStorage.setItem('qtdeProd', itemCarrinho+1); // quantidade de produtos diferentes no carrinho
        localStorage.setItem('lastProd', itemCarrinho);
        itemCarrinho++;
        contCarrinho.innerHTML = itemCarrinho;
    }
    attContCarrinho();
    closeModal();
    showNotif("Produto adicionado ao carrinho.", true);
    setTimeout(closeNotif, 6000);
}
function attContCarrinho() {
    if (localStorage.qtdeProd > 0) {
        contCarrinho.innerHTML = localStorage.qtdeProd;
    }
    else {
        contCarrinho.innerHTML = 0;
    }
}
function prevPgTab() {
    if (pag > 1) {
        if (buscaProd.value != undefined) {
            listarProdutos(pag - 1, buscaProd.value);
        }
        else {
            listarProdutos(pag - 1);
        }
    }
}
function nextPgTab() {
    if (pag < Math.floor(totalProd / 10)) {
        if (buscaProd.value != undefined) {
            listarProdutos(pag + 1, buscaProd.value);
        }
        else {
            listarProdutos(pag + 1);
        }
    }
}
function closeModal() {
    if (document.getElementById('prodModal') != null) {
        prodExp.classList.remove("centro");
    }
}